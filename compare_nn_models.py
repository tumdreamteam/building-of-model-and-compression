# Program computes output of a fully connected
# neural netwrok for the given inputs and NN
# weights and biases.
# The model is extracted from a zip file.
# 
# Program does matrix multiplication and 
# addition according to the formula:
#  Y = output_activfun(Wh2o*H+Bh2o) 
#  H = leaky_relu(Wi2h*X+Bi2h)
#
#  where (size = [a x b] where: a - num of rows, b - number of coloumns):
#   Y    - outputs of the NN [num_output_nodes x num_input_samples]
#   Wh2o - weights from the hidden to the output layer [num_output_nodes x num_hidden_nodes]
#   Bh2o - biases for the hidden to the output layer [num_output_nodes x 1]
#   output_activfun - output acitvation function (sigmoid for bin & multi NNs or exp for reg NNs)
#   H    - outputs of the hidden layer [num_hidden_nodes x num_input_samples]
#   Wi2h - weights from the inputs to the hidden layer [num_hidden_nodes x num_features]
#   Bi2h - biases for the inputs to the hidden layer [num_hidden_nodes x 1]
#   leaky_relu - acitvation function of the hidden layer in all types of NN
#   X    - inputs of the NN [num_features x num_input_samples]
#
# !IMPORTANT!: info about the storage of the model has to be
#			   input manually to match the storage procedure used! 
#
# Author: Uros Hudomalj
# Last revision: 29.12.2018

#################      IMPORTS      #################
import tensorflow as tf
from tensorflow import keras
import numpy as np
import csv
import matplotlib.pyplot as plt
import pickle
import zipfile
import zlib
import math
#######    CONSTANTS and GLOBAL VARIABLES    ########

num_input_samples = 100000							# number of inputs for which the outputs of the NN are calculated
num_features = 4*24*3								# number of features of the inputs
num_hidden_nodes = 2*num_features					# number of hidden nodes of the NN
nums_output_nodes = {'bin':2, 'multi':4, 'reg':1}	# number of output nodes of the NN for each stage
BYTE_NUM = 2				# number of bytes used for storage of a single value
BIT_NUM = BYTE_NUM*8		# number of bits used for storage of a single value - has to be a multiple of 8

ENABLE_SVD = False		# if true, SVD was also used for copression
K = 1.01				# used SVD compression ratio of m*n/(m*r + r + r*n) - used just on Wi2h [m x n]
r = int(191.78/K)		# in our example m=2n => 2n**2/(r*(3n+1)) and n=288 => r = 191.78/K
if r > min([num_hidden_nodes, num_features]): r = min([num_hidden_nodes, num_features])		# a full/lossless reconstruction possible - more then that not!

INPUTS = 4	                                        # number of different types of variables to the NN (4: bar, temp, RH and rainfall; or 6: + wind speed and wind direction )

DAYS_BACK = 3                                       # number of days looking back for prediction
HOURS_AHEAD = 24                                    # number of hours ahead for which rainfall amounts are predicted

HOURS_IN_A_DAY = 24
NN_OUTPUTS = HOURS_AHEAD                            # number of outputs of NN
NN_INPUTS = DAYS_BACK * HOURS_IN_A_DAY * INPUTS     # number of inputs of NN (DAYS_BACK*24-1 past values and current value)

HIDDEN_NODES = int(NN_INPUTS * 2)                   # number of nodes in hidden layer (multiple of NN_INPUTS)

MAX_RAINFALL_AMOUNT = 62.4                          # representing normalized value of 1 in rainfall amount
RAINFALL_RESOLUTION = 0.1							# accuracy of the measurements of rainfall

LARGEST_ERROR_PER = 0.1								# parameter for a metrices for largest error average

COMPARE_ALL_MODELS = False							# if true, plot a histogram of correlation between results obtained by quantized model, model saved in csv and model saved in native Tensorflow form
COMPARE_QUANT_TENS_MODELS = False or COMPARE_ALL_MODELS
COMPARE_CSV_TENS_MODELS = False or COMPARE_ALL_MODELS
PLOT_COMPARISON_VS_REAL = False or COMPARE_ALL_MODELS
OUTPUT_HOURLY_COMPARE = True 						# if true plots hourly comparison of some metrices for different models
ACC_HOURLY_COMPARE = True 							# if true plots hourly comparison between different models for each stage
ACC_HOURLY_COMPARE_DETAIL = True 					# if true plots also hourly comparison between different models for all stages of accuracies/mae for all labels

LOAD_INPUTS_PATH = 'whole_model/nn_data.pk'			# path to stored values of inputs for testing purposes

#################     FUNCTIONS     #################

# get_inputs(LOAD_INPUTS_PATH)
#  returns the inputs
def get_inputs(LOAD_INPUTS_PATH):
	print("Loading nn_data_in from saved values.")
	with open(LOAD_INPUTS_PATH, 'rb') as fi:
		# load data from the file
		nn_data_in = pickle.load(fi)
		nn_data_out = pickle.load(fi)
	print("Finished loading nn_data_out.")

	nn_data_in = np.transpose(nn_data_in)

	return nn_data_in[:,0:num_input_samples]

# get_ref_outputs(LOAD_INPUTS_PATH)
#  returns the reference outputs
def get_ref_outputs(LOAD_INPUTS_PATH):
	print("Loading nn_data_out from saved values.")
	with open(LOAD_INPUTS_PATH, 'rb') as fi:
		# load data from the file
		nn_data_in = pickle.load(fi)
		nn_data_out = pickle.load(fi)
	print("Finished loading nn_data_out.")

	nn_data_out = np.transpose(nn_data_out)

	return nn_data_out[:,0:num_input_samples]

# read_csv_model(model_path)
#  reads and returns the weights and biases of the model
def read_csv_model(model_path):

	with open(model_path, 'r') as csv_file:
		data = list(csv.reader(csv_file, delimiter=';'))

	Wi2h = []
	Bi2h = []
	Wh2o = []
	Bh2o = []

	# from th input CSV divide data into the right matrices
	idx = 0
	for row in data:
		if row[0] == 'Weights from input to hidden layer.':
			idx = 1
		elif row[0] == 'Bias from input to hidden layer.':
			idx = 2
		elif row[0] == 'Weights from hidden to output layer.':
			idx = 3
		elif row[0] == 'Bias from hidden to output layer.':
			idx = 4
		else:
			if idx == 1:
				Wi2h.append(row)
			elif idx == 2:
				Bi2h.append(row)
			elif idx == 3:
				Wh2o.append(row)
			elif idx == 4:
				Bh2o.append(row)

	# Covert data lists to numpy arrays
	Wi2h = np.array([ np.asarray(xi, dtype=float) for xi in Wi2h ])
	Bi2h = np.array([ np.asarray(xi, dtype=float) for xi in Bi2h ])
	Wh2o = np.array([ np.asarray(xi, dtype=float) for xi in Wh2o ])
	Bh2o = np.array([ np.asarray(xi, dtype=float) for xi in Bh2o ])

	# Transpose the matrices to get the right dimenssions
	Wi2h = np.transpose(Wi2h)
	Bi2h = np.transpose(Bi2h)
	Wh2o = np.transpose(Wh2o)
	Bh2o = np.transpose(Bh2o)

	return [Wi2h, Bi2h, Wh2o, Bh2o]

# leaky_relu(x, alpha)
def leaky_relu(x, alpha = 0.2):
	x[x<0] = x[x<0]*alpha			# multiply the negative values with coeficient alpha
	return x

# softmax(x)
def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    sums = np.sum(np.exp(x), axis=0)
    return np.exp(x) / sums[None,:]

# calc_outputs(Wi2h, Bi2h, Wh2o, Bh2o, X, stage)
#  calculate the outputs of the NN according
#  to the formulas:
#   Y = output_activfun(Wh2o*H+Bh2o) 
#   H = leaky_relu(Wi2h*X+Bi2h)
#  use parameter stage to choose appropriate activation function for output nodes
def calc_outputs(Wi2h, Bi2h, Wh2o, Bh2o, X, stage):

	# calculate the values of the hidden layer
	calc = np.dot(Wi2h, X)				# caluclate dot product of matrices

	calc = calc + Bi2h					# add bias to each element (to each element of a row add corresponding bias)

	alpha = 0.2 						# alpha parameter of the leaky relu (in tensorflow the default is 0.2 according to https://www.tensorflow.org/api_docs/python/tf/nn/leaky_relu?)
	calc = leaky_relu(calc,alpha)		# apply the leaky_relu function to each element

	# calculate the outputs of the NN
	calc = np.dot(Wh2o, calc)			# caluclate dot product of matrices
	calc = calc + Bh2o					# add bias to each element (to each element of a row add corresponding bias)

	# define the output activation function according to the type of the NN
	if stage != 'reg':				
		# in case of classification NN, the output activation function is softmax
		calc = softmax(calc)			# apply output activation function to each element
	else:
		# in case of the regression NN, thr output activation function is exp
		calc = np.exp(calc)				# apply output activation function to each element

	return calc

# recreate_model_from_bin_SVD(data, stage)
#  returns matrices Wi2h, Bi2h, Wh2o, Bh2o of a NN model
#  matrices are saved in 16 'dynamic' floating point
#  'dynamic' means that each matrix has its own number of bits for int - denoted by the first byte
#  matrix Wi2h is written as U[m x r], s[r x 1], VT[r x n] in from SVD, where r is defined at the start of this file
def recreate_model_from_bin_SVD(data, stage):

	num_output_nodes = nums_output_nodes[stage]			# get the number of output nodes according to the stage type

	if ENABLE_SVD:
		[U, s, VT, Bi2h, Wh2o, Bh2o] = [[],[],[],[],[],[]]	# initialize empty list for matrices
		matrices = [U, s, VT, Bi2h, Wh2o, Bh2o]				# join them under common name
		# define the sizes of each matrix [num_row,num_columns]
		matrices_sizes = [[num_hidden_nodes, r],
						  [r, 1],
						  [r, num_features],
						  [num_hidden_nodes, 1],
						  [num_output_nodes, num_hidden_nodes],
						  [num_output_nodes, 1]]
	else:
		[Wi2h, Bi2h, Wh2o, Bh2o] = [[],[],[],[]]	# initialize empty list for matrices
		matrices = [Wi2h, Bi2h, Wh2o, Bh2o]				# join them under common name
		# define the sizes of each matrix [num_row,num_columns]
		matrices_sizes = [[num_hidden_nodes, num_features],
						  [num_hidden_nodes, 1],
						  [num_output_nodes, num_hidden_nodes],
						  [num_output_nodes, 1]]						

	bin_data = data

	i = 0 									# current byte from the file which wasn't yet read & stored
	for mi, matrix in enumerate(matrices):
		N = bin_data[i]						# number of bits used for the integer part of the value
		i = i + 1 							# increase the counter to next byte

		# build a matrix of the appropriate size and fill it with values represented by corresponding bytes
		for row in range(matrices_sizes[mi][0]):
			matrices[mi].append([])			# make a new row

			for col in range(matrices_sizes[mi][1]):
				# get all the bytes for one value
				byte_val = []
				for Bi in range(BYTE_NUM):
					byte_val.append(bin_data[i])	# get 8 bits
					i = i + 1						# move the bit index

				# ### NEW ALGORITHM TO RECREATE FLOATS FROM BYTES
				tmp_int = int.from_bytes(byte_val, byteorder='big', signed=False)
				if tmp_int>(1<<(BIT_NUM-1)):
					tmp_int -= (1<<BIT_NUM)

				float_val_2 = tmp_int / (float)(1<<(BIT_NUM-N-1))
				matrices[mi][row].append(float_val_2)	# append to the matrix calculated value
				# ### END OF NEW ALGORITHM TO RECREATE FLOATS FROM BYTES

				# float_val = 0			# float fixed point value from the BIT_NUM bits
				# sign = 1 				# containing the sign of the value
				# bit_count = BIT_NUM		# counter for the current BIT_NUM bit value
				
				# # from the 2 bytes form a float by multiplying each bit with corresponding factor of 2**ex
				# for ex in range(N,-(BIT_NUM-N-1)-1, -1):		# decending sequence from N to -(BIT_NUM-N-1)
					
				# 	# get the current bit/digit
				# 	if bit_count == BIT_NUM:
				# 		# if the MSB is 1, the value is negative
				# 		digit = byte_val[0] & (1<<(8-1))		# isolate the MSB (mask it)
				# 		digit = (digit >> (8-1))				# shift the isolated bit out
				# 		if digit == 1:
				# 			# if the value is negative, get the abs value
				# 			for j, Bj in enumerate(byte_val):
				# 				if j==BYTE_NUM-1:
				# 					byte_val[j] = abs(Bj - 256)	# there is no +1 for the last byte
				# 				else:
				# 					byte_val[j] = abs(Bj - 256 + 1)
				# 					# abs on a SINGLE value is a lot (over all results 3x) faster then using np.abs
				# 			sign = -1
				# 	else:
				# 		tmp = bit_count%8-1
				# 		if tmp<0: tmp = 7
				# 		digit = byte_val[BYTE_NUM-int(math.floor((bit_count-1)/8+0.001))-1] & (1<<tmp)	# isolate the current bit (mask it)
				# 		digit = (digit >> tmp)					# shift the isolated bit out
					
				# 		float_val = float_val + digit*2**(ex)	# add current digit multiplied with factor 2**ex
				# 		#print("sum: {} c dig: {}, c ex: {}, bc {}, tmp {}".format(float_val, digit, ex, bit_count, tmp))
					
				# 	bit_count = bit_count - 1 					# move to the next (lower) bit

				# print("Orig: {}".format(sign*float_val))
				# print("New: {}".format(float_val_2))

				# matrices[mi][row].append(sign*float_val)	# append to the matrix calculated value

		# Covert data lists to numpy arrays
		matrices[mi] = np.array([ np.asarray(xi, dtype=float) for xi in matrices[mi] ])

	if ENABLE_SVD:
		# join the first 3 matrices U, s, VT into Wi2h
		matrices[1] = np.transpose(matrices[1])[0]		# to transpose s into vector
		Wi2h = np.dot(matrices[0], np.dot(np.diag(matrices[1]), matrices[2]))
		#print(Wi2h)
		#exit()
		return [Wi2h, matrices[3], matrices[4], matrices[5]]
	else:
		#print(matrices[0][:10,0])
		#exit()
		return matrices


# plot_corr_and_error(X, Y, X_label, Y_label, SUBPLOT=0, BINS_RESOLUTION=0.1, LARGEST_ERROR_PER=1)
#  plots correlation Y vs X and corresponding error of X-Y
#  prints also 4 metrices:
#   "Max absolute error"
#	"Average absolute error"
#	"Aae of false values"
#	"Aae of largest LARGEST_ERROR_PER% false values"
#
#  X, Y - np arrays to plot. If lists of np arrays are given, all of them are ploted
#			in that case X_label and Y_label must match the dimensions
#  SUBPLOT - if 0, no subplots are used
#		     if 1, corr and error are plotted together
#			 if 2, all corr and errors are plotted together on same subplots
def plot_corr_and_error(X, Y, X_label, Y_label, SUBPLOT=0, BINS_RESOLUTION=0.1, LARGEST_ERROR_PER=1):
	x_max, y_max, hist_max = [0, 0, 0]

	sub_col = 1
	if isinstance(X, list):
		sub_col = len(X)	# set the number of columns for subplots equal to the number of signals you want to plot
		# set axis limits
		for si in range(0, sub_col):
			x_max = np.max([x_max, np.max(X[si])])
			y_max = np.max([y_max, np.max(Y[si])])
			hist_max = np.max([hist_max, np.max(np.abs(X[si]-Y[si]))])
	else:
		if SUBPLOT==2: SUBPLOT=1	# cannot plot multiple subplots if there is not enough inputs
		x_max = np.max(X)
		y_max = np.max(Y)
		hist_max = np.max(np.abs(X-Y))
	
	x_axis = (0, x_max)
	y_axis = (0, y_max)
	x_hist = (0, hist_max)

	for si in range(0, sub_col):
		# Plotting the correlations
		if SUBPLOT!=0:
			if SUBPLOT==1:
				plt.subplot(2, 1, 1)
			elif SUBPLOT==2:
				plt.subplot(2, sub_col, si+1)
		if isinstance(X, list):
			plt.scatter(X[si],Y[si])
			plt.xlabel(X_label[si])
			plt.ylabel(Y_label[si])
		else:
			plt.scatter(X,Y)
			plt.xlabel(X_label)
			plt.ylabel(Y_label)
		plt.axis('equal')
		plt.xlim(x_axis)
		plt.ylim(y_axis)
		_ = plt.plot([0, MAX_RAINFALL_AMOUNT], [0, MAX_RAINFALL_AMOUNT])
		if SUBPLOT==0: plt.show(block=True)

		# Plotting of the errors on testing data
		if isinstance(X, list):
			error = (X[si]-Y[si])
		else:
			error = (X-Y)
		error = np.abs(error)
		if SUBPLOT!=0:
			if SUBPLOT==1:
				plt.subplot(2, 1, 2)
			elif SUBPLOT==2:
				plt.subplot(2, sub_col, sub_col+si+1)
		bins=int(np.ceil(np.max(error)/BINS_RESOLUTION))
		plt.hist(error, bins=bins, weights=np.ones(len(error))*100 / len(error))
		plt.xlim(x_hist)
		plt.yscale('log', nonposy='clip')
		plt.xlabel("Error [mm]")
		_ = plt.ylabel("Occurances [%]")

		# Printing the metrices
		print("  Max absolute error: {} mm".format(np.max(error)))
		print("  Average absolute error: {} mm".format(np.average(error)))
		false_val = error[error>RAINFALL_RESOLUTION]		# extract just the falsely predicted values (above resolution of rainfall)
		if len(false_val)>0:
			print("  Average absolute error of false values: {} mm".format(np.average(false_val)))
			max_error = np.sort(false_val)
			N = int(np.ceil(len(max_error)*LARGEST_ERROR_PER/100))
			print("  Average absolute error of largest {} rainfalls: {} mm".format(N, np.average(max_error[-N:])))
		else:
			print("  Perfect match - taking into account the accuracy of data.")

		if SUBPLOT==0 or SUBPLOT==1: plt.show(block=True)
	if SUBPLOT==2: plt.show(block=True)

#################      PROGRAM      #################

print("Starting program.")

model_path = 'whole_model/hour_{hour}_model/{stage}_{form}'		# path to the NN for hour {hour} and NN of stage {stage} in form of {form} - form is either QUANT_FORM, CSV_FORM or TENS_FORM
nn_stages = ['bin', 'multi', 'reg']								# stages/types of NN for a model for an hour
QUANT_FORM = 'nn_quant.bin'										# quantized form of the model weights
CSV_FORM = 'nn_weights.csv'										# reference/accurate model weights in CSV form
TENS_FORM = 'model'												# nn model is saved in a format native to Tensorflow

### Constants for setting up the Tensorflow models
output_nodes = [2, 4, 1]											# number of output nodes for each stage of the model of an hour
output_activfun = [tf.nn.softmax,									# output activation function for each stage of the model of an hour
				   tf.nn.softmax,
				   tf.math.exp
				   ]		
model_optimizers = [tf.train.AdamOptimizer(learning_rate=0.0001),	# optimizer for each stage of the model of an hour
				    tf.train.AdamOptimizer(learning_rate=0.0001),
				    tf.train.RMSPropOptimizer(learning_rate=0.0001)]
model_losses = ['sparse_categorical_crossentropy',					# loss function for each stage of the model of an hour
				'sparse_categorical_crossentropy',
				'mae']
model_metricses = ['accuracy', 'accuracy', 'mae']					# metricses for each stage of the model of an hour
###

print("Getting inputs.")
# Get the inputs
X = get_inputs(LOAD_INPUTS_PATH)

print("Getting the real/reference outputs.")
# Get the real values which happend
Y_ref = get_ref_outputs(LOAD_INPUTS_PATH)
# Derive the real outputs for each stage of the model
Y_ref_bin = Y_ref.copy()
Y_ref_multi = Y_ref.copy()
Y_ref_reg = Y_ref.copy()	# later have to take into account just the values when Y_ref_bin = 1
Y_ref_bin[Y_ref_bin>0] = 1	# set values when it rains to 1, else keep at 0
# devide rainfall amounts into intervals (later have to take into account just the values when Y_ref_bin = 1)
Y_ref_multi[Y_ref>0.001/MAX_RAINFALL_AMOUNT] = 0
Y_ref_multi[Y_ref>1.001/MAX_RAINFALL_AMOUNT] = 1
Y_ref_multi[Y_ref>2.001/MAX_RAINFALL_AMOUNT] = 2
Y_ref_multi[Y_ref>3.001/MAX_RAINFALL_AMOUNT] = 3

Y_ref_stages = {'bin':Y_ref_bin, 'multi':Y_ref_multi, 'reg':Y_ref_reg}

# Variables for model evaluation
model_stage_accuracies = {'bin':{'tens':[], 'quant':[]},			# accuracies/mae of each stage of the model
						  'multi':{'tens':[], 'quant':[]},			# for Tensorflow and quantized models
						  'reg':{'tens':[], 'quant':[]}				# for each hour
						 }											# accuracies are: overall performance and percentage of
						 											# correctly classified outputs for each possible class

errors_quant = []			# for evaluation of errors of quant model
errors_tens = []			# for evaluation of errors of tensorflow model

# Evaluate the compressed model for each hour and each its NN stage
# and compare it to the reference/most accurate model 
for hour in range(1,HOURS_AHEAD+1):
	print("Evaluating for hour {}.".format(hour))

	# variables containing outputs of each model form
	Y_csv = [0, 0, 0]
	Y_tens = [0, 0, 0]
	Y_quant = [0, 0, 0]

	# Evaluate each NN stage for an hour
	for i_stage, stage in enumerate(nn_stages):
		print(" Evaluating for stage {}.".format(stage))

		print("  Getting reference/accurate model weights and biases from CSV.")
		# Read the model weights and biases
		Wi2h, Bi2h, Wh2o, Bh2o = read_csv_model(model_path.format(hour=hour, stage=stage, form=CSV_FORM))

		print("  Computing the reference/accurate model outputs based on CSV.")
		# Compute the reference outputs based on CSV
		Y_csv[i_stage] = calc_outputs(Wi2h, Bi2h, Wh2o, Bh2o, X, stage)

		print("  Computing the reference/accurate model outputs based on Tensorflow.")
		# Compute the reference outputs based on Tensorflow
		#  Setup Tensorflow model
		#   Setup layers of the model
		model = keras.Sequential([
			keras.layers.Flatten(input_shape=(NN_INPUTS,)),
			keras.layers.Dense(HIDDEN_NODES, activation=tf.nn.leaky_relu),
			keras.layers.Dense(output_nodes[i_stage], activation=output_activfun[i_stage])
		])
		#   Compiling model
		model.compile(optimizer=model_optimizers[i_stage],
					  loss=model_losses[i_stage],
					  metrics=[model_metricses[i_stage]])
		#   Load saved weights of the model
		model.load_weights(model_path.format(hour=hour, stage=stage, form=TENS_FORM))
		#  Compute the outputs based on Tensorflow
		tmp = np.transpose(X)						# transposing is needed to get the appropriate form for the inputs to model.predict()
		tmp = model.predict(tmp)
		Y_tens[i_stage] = np.transpose(tmp)			# transposing is needed to get dimensions as in stated in the beginning

		print("  Getting quantized weights and biases.")
		# Get the quantized model weights and biases
		zf = zipfile.ZipFile('whole_model.zip')
		data = zf.read(model_path.format(hour=hour, stage=stage, form=QUANT_FORM))
		Wi2h, Bi2h, Wh2o, Bh2o = recreate_model_from_bin_SVD(data, stage)
		print("  Computing the quantized model outputs.")
		# Compute the quantized model outputs
		Y_quant[i_stage] = calc_outputs(Wi2h, Bi2h, Wh2o, Bh2o, X, stage)

		# In case of a classification NN, transform outputs to classes
		if stage != 'reg':
			print("   Classifying the outputs.")
			# classify the output to the value with the highest probability
			Y_csv[i_stage] = np.argmax(Y_csv[i_stage], axis=0)
			Y_tens[i_stage] = np.argmax(Y_tens[i_stage], axis=0)
			Y_quant[i_stage] = np.argmax(Y_quant[i_stage], axis=0)
		elif stage=='reg':
			Y_csv[i_stage] = Y_csv[i_stage][0]
			Y_tens[i_stage] = Y_tens[i_stage][0]
			Y_quant[i_stage] = Y_quant[i_stage][0]

		# Evaluate the accuracies/mae of each stage for Tens and Quant models for each hour
		Y_eval =  {'tens':Y_tens[i_stage], 'quant':Y_quant[i_stage]}
		dif_classes = {'bin':2, 'multi':4, 'reg':1}
		evaluation = {'tens':[], 'quant':[]}
		for model in Y_eval:
			# renaming for shorter code
			Yr = Y_ref_stages[stage][hour-1,:]		# real/reference values for current hour and stage
			Ye = Y_eval[model]
			if stage == 'multi':
				# extract just the values when there is rain
				idx = Y_ref_stages['bin'][hour-1,:]==1
				Yr = Yr[idx]
				Ye = Ye[idx]
			elif stage == 'reg':
				# extract just the values when there is rain and it is classified into the last interval
				idx = np.logical_and(Y_ref_stages['bin'][hour-1,:]==1, Y_ref_stages['multi'][hour-1,:]==3)
				Yr = Yr[idx]
				Ye = Ye[idx]

				diff = np.abs(Ye - Yr)
				evaluation[model].append(np.average(diff)*MAX_RAINFALL_AMOUNT)
				print("Evaluation for model {}, hour {} and stage {} is {}".format(model, hour, stage, evaluation[model]))
				model_stage_accuracies[stage][model].append(evaluation[model])
				continue

			equal_samples = Ye==Yr 					# correctly classified samples

			all_samples = len(Yr)					# length of all the samples
			if all_samples==0: all_samples = 1		# to prevent devision by 0
			corr = Ye[equal_samples]				# get correctly classified values
			evaluation[model].append(len(corr)/all_samples*100)			# percentage of correctly classified classes

			for cl in range(0,dif_classes[stage]):						# evaluate correctness for each label/class
				equal_to_class = Yr==cl
				samples_cl = len(Yr[equal_to_class])					# length of all samples of class cl
				if samples_cl==0: samples_cl = 1						# to prevent devision by 0
				corr_cl = Ye[np.logical_and(equal_samples, equal_to_class)]			# correctly classified class cl
				evaluation[model].append(len(corr_cl)/samples_cl*100)	# percentage of correctly classified class cl

			print("Evaluation for model {}, hour {} and stage {} is {}".format(model, hour, stage, evaluation[model]))
			model_stage_accuracies[stage][model].append(evaluation[model])

	# Evaluate together the model output by joining the stages' outputs
	nn_data_out_pred = []
	Ys = [Y_csv, Y_tens, Y_quant]

	# combine the results = non zero rainfalls change with corresponding regression NN amounts
	tmp_size = X.shape[1]
	for i in range(tmp_size):			# go through all the inputs/outputs
		nn_data_out_pred.append([])

		# for each different source of outputs
		for Y in Ys:
			# if rain is predicted - from the bin (0) NN
			if Y[0][i]>0:
				# if the rainfall is from the last interval - from the multi (1) NN
				if Y[1][i]==3:
					# and set the appropriate regression value to the output - from the reg (2) NN
					tmp = Y[2][i]		# [0] is needed because of my legacy error/different style of writing code
				else:
					# turn class label into average value for the interval
					tmp = (Y[1][i]+0.5)/MAX_RAINFALL_AMOUNT
				nn_data_out_pred[i].append(tmp)
			else:
				nn_data_out_pred[i].append(0)

	# Covert data lists to numpy arrays
	nn_data_out_pred = np.array([ np.asarray(xi, dtype=float) for xi in nn_data_out_pred ])

	# Plotting the difference between different models and the real values
	#  CSV vs Tensorflow
	if COMPARE_CSV_TENS_MODELS:
		print(" Graphical representation of CSV vs Tensorflow for hour {hour}.".format(hour=hour))
		plot_corr_and_error(nn_data_out_pred[:,0]*MAX_RAINFALL_AMOUNT,
							nn_data_out_pred[:,1]*MAX_RAINFALL_AMOUNT,
							'CSV [mm]',
							'Tensorflow [mm]',
							SUBPLOT=1, BINS_RESOLUTION=0.5, LARGEST_ERROR_PER=LARGEST_ERROR_PER)
	#  QUANT vs Tensorflow
	if COMPARE_QUANT_TENS_MODELS:
		print(" Graphical representation of Quantized vs Tensorflow for hour {hour}.".format(hour=hour))
		plot_corr_and_error(nn_data_out_pred[:,2]*MAX_RAINFALL_AMOUNT,
							nn_data_out_pred[:,1]*MAX_RAINFALL_AMOUNT,
							'Quant [mm]',
							'Tensorflow [mm]',
							SUBPLOT=1, BINS_RESOLUTION=0.5, LARGEST_ERROR_PER=LARGEST_ERROR_PER)

	X_quant = nn_data_out_pred[:,2]*MAX_RAINFALL_AMOUNT
	Y_quant = Y_ref[hour-1,:]*MAX_RAINFALL_AMOUNT
	error_quant = np.abs(X_quant - Y_quant)
	errors_quant.append(error_quant)

	X_tens = nn_data_out_pred[:,1]*MAX_RAINFALL_AMOUNT
	Y_tens = Y_ref[hour-1,:]*MAX_RAINFALL_AMOUNT
	error_tens = np.abs(X_tens - Y_tens)
	errors_tens.append(error_tens)

	x_axis = (0, np.max([X_tens, X_quant]))
	y_axis = (0, np.max([Y_tens, Y_quant]))
	x_hist = (0, np.max([error_tens, error_quant]))

	BINS_RESOLUTION = 0.5

	# Comparison of accuracy between quant and tens vs real
	if PLOT_COMPARISON_VS_REAL:
		#  QUANT vs Real and Tensorflow vs Real
		print("Graphical representation of Quantized vs Real for hour {hour}.".format(hour=hour))
		plot_corr_and_error([X_quant, X_tens],
							[Y_quant, Y_tens],
							['Quant [mm]', 'Tensorflow [mm]'],
							['Real [mm]', 'Real [mm]'],
							SUBPLOT=2, BINS_RESOLUTION=0.5, LARGEST_ERROR_PER=LARGEST_ERROR_PER)

#print(model_stage_accuracies)

# Compare accuracies/mae of both models for all hours
if ACC_HOURLY_COMPARE:
	print("Comparing accuracies/mae of models.")

	for stage in model_stage_accuracies:
		i=0
		for model in model_stage_accuracies[stage]:
			col = len(model_stage_accuracies[stage][model][0])
			for j in range(col):
				y_data = [row[j] for row in model_stage_accuracies[stage][model]]
				if ACC_HOURLY_COMPARE_DETAIL: plt.subplot(col,1,j+1)
				if j==0:
					plt.title('Overall accuracy')
					print("Accuracy/MAE for stage {} for model {} is\n avg: {:.2f} min: {:.2f} max: {:.2f}".format(stage, model, sum(y_data)/len(y_data), min(y_data), max(y_data)))
				else:
					plt.title('Accuracy of label {}'.format(j-1))
				plt.scatter(range(1, HOURS_AHEAD+1), y_data, label='{}'.format(model))
				plt.xlabel('Hours')
				plt.xticks(range(1, HOURS_AHEAD+1, 2))
				if stage=='reg':
					plt.ylabel('MAE')
				else:
					plt.ylabel('Accuracy')
					plt.ylim(min(min(y_data),plt.ylim()[0])-5,100)
				plt.suptitle("Accuracies/MAE for stage {} for all hours".format(stage))
				if ACC_HOURLY_COMPARE_DETAIL:
					plt.subplots_adjust(hspace=1.5, wspace=1.5)
				else:
					break
		plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), ncol=2)
		plt.show(block=True)

# Compare errors of both models (vs real) for all hours
if OUTPUT_HOURLY_COMPARE:
	print("Comparing errors of models.")

	all_errors = {'tens':errors_tens, 'quant':errors_quant}
	all_errors_metrices = {'tens':{'Mae':[], 'Aae':[], 'AaeF':[], 'AaeL':[]},
						   'quant':{'Mae':[], 'Aae':[], 'AaeF':[], 'AaeL':[]}}

	for hour in range(1, HOURS_AHEAD+1):
		for model_name in all_errors:
			error = all_errors[model_name][hour-1]
			
			all_errors_metrices[model_name]['Mae'].append(np.max(error))
			all_errors_metrices[model_name]['Aae'].append(np.average(error))
			
			false_val = error[error>RAINFALL_RESOLUTION]		# extract just the falsely predicted values (above resolution of rainfall)
			if len(false_val)>0:
				all_errors_metrices[model_name]['AaeF'].append(np.average(false_val))
				max_error = np.sort(false_val)
				N = int(len(max_error)*LARGEST_ERROR_PER/100)		# get largest top LARGEST_ERROR_PER% values
				all_errors_metrices[model_name]['AaeL'].append(np.average(max_error[-N:]))

	metrices_full_name = {'Mae':"Max absolute error",
						  'Aae':'Average absolute error',
						  'AaeF':"Aae of false values",
						  'AaeL':"Aae of largest {}% false values".format(LARGEST_ERROR_PER)}
	
	for i, metric in enumerate(all_errors_metrices['tens']):
		plt.subplot(2,2,i+1)
		plt.title('{} [mm] for each hour'.format(metrices_full_name[metric]))
		for model_name in all_errors_metrices:
			plt.scatter(range(1, HOURS_AHEAD+1), all_errors_metrices[model_name][metric], label='{}'.format(model_name))
		plt.xlabel('Hours')
		plt.xticks(range(1, HOURS_AHEAD+1, 2))
		plt.ylabel('{} [mm]'.format(metric))
		plt.subplots_adjust(hspace=.5)
	plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.1), ncol=2)
	plt.show(block=True)
