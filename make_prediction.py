# Program is used to make rainfall
# predictions based on built model.
# Predictions are combined with
# readings from sensors of soil
# moisture content. Based on this
# program advises whether there is
# need for watering the plants.
#
# The model is extracted from a ZIP
# file and reconstructed out of the
# compressed version. Outputs of
# the model are calculated and used
# to advise the user whether the
# plants need watering.
#
# Author: Uros Hudomalj
# Last revision: 21.1.2019

#################      IMPORTS      #################
import numpy as np
import pickle
import zipfile
import zlib

import math
import time 	#for testing

#######    CONSTANTS and GLOBAL VARIABLES    ########

### FOR WATERING ADVISE

MIN_MOIST_ADC = 980				# minimal moisture of the soil (in output values of the ADC)
MAX_MOIST_ADC = 360				# maximal moisture of the soil (in output values of the ADC)
WATER_NEEDED_PER_DAY_MM = 0.6	# amount of water plants need per day in mm 
								#   amount / based on sources:
								#   - 0.25-0.4 (for plant of 10cmX10cm) Marc van Iersel, Stephanie Burnett and Jongyun Kim; https://www.greenhousemag.com/article/gmpro-0310-water-plants-automating-irrigation/
								#   - 0.35-0.55; https://www.wikihow.com/Determine-How-Much-Water-Plants-Need
								#   - 0.25-0.65 for corn; https://articles.extension.org/pages/14080/corn-water-requirements & https://harvesttotable.com/how_to_grow_sweet_corn/ as well as http://extension.missouri.edu/scott/documents/Ag/Irrigation/Corn-Irrigation-and-Water-Use.pdf

MAX_OPTIMAL_MOIST_ADC = (MIN_MOIST_ADC - MAX_MOIST_ADC)/4 + MAX_MOIST_ADC	# assumed maximal still optimal levels of moisture in the soil (in output values of the ADC)
MIN_OPTIMAL_MOIST_ADC = 3*(MIN_MOIST_ADC - MAX_MOIST_ADC)/4 + MAX_MOIST_ADC	# assumed minimal still optimal levels of moisture in the soil (in output values of the ADC)

MAX_WATER_RESERVE = 3*24		# constant containing for how many hours in advance is the soil capable of storing water (has enough water reserves)
MOIST_TO_RAINFALL = lambda x: (MAX_WATER_RESERVE*WATER_NEEDED_PER_DAY_MM/24 - WATER_NEEDED_PER_DAY_MM/24)/(MAX_OPTIMAL_MOIST_ADC - MIN_OPTIMAL_MOIST_ADC)*(x-MAX_OPTIMAL_MOIST_ADC) + MAX_WATER_RESERVE*WATER_NEEDED_PER_DAY_MM/24
# correlation between the moisture content in the soil and amount of rainfall - dependent on the type of soil and temperature but not on the plant (taken into account by the daily amount of water needed)
#  transforms how much water in mm were lost/gained if moisture content (in output adc value) changed for some delta amount
#  assumed as follows: MIN_OPTIMAL_MOIST contains enough water needed by the plant for an hour (that is WATER_NEEDED_PER_DAY_MM/24), MAX_OPTIMAL_MOIST contains on the other hand enough water reservs for the next MAX_WATER_RESERVE hours (MAX_WATER_RESERVE*WATER_NEEDED_PER_DAY_MM/24)

# get_current_moist()
#  returns current level of moisture (in output values of the ADC)
def get_current_moist():
	return 234

### ###

num_input_samples = 1								# number of the inputs for which the outputs of the NN are calculated = 1 - just one prediction is made
num_input_sample = 194 #194								# number of the input for which the outputs of the NN are calculated - for manual testing
num_features = 4*24*3								# number of features of the inputs
num_hidden_nodes = 2*num_features					# number of hidden nodes of the NN
nums_output_nodes = {'bin':2, 'multi':4, 'reg':1}	# number of output nodes of the NN for each stage
BYTE_NUM = 2				# number of bytes used for storage of a single value
BIT_NUM = BYTE_NUM*8		# number of bits used for storage of a single value - has to be a multiple of 8

ENABLE_SVD = False		# if true, SVD was also used for copression
K = 1.01				# used SVD compression ratio of m*n/(m*r + r + r*n) - used just on Wi2h [m x n]
r = int(191.78/K)		# in our example m=2n => 2n**2/(r*(3n+1)) and n=288 => r = 191.78/K
if r > min([num_hidden_nodes, num_features]): r = min([num_hidden_nodes, num_features])		# a full/lossless reconstruction possible - more then that not!

INPUTS = 4	                                        # number of different types of variables to the NN (4: bar, temp, RH and rainfall; or 6: + wind speed and wind direction )

DAYS_BACK = 3                                       # number of days looking back for prediction
HOURS_AHEAD = 24                                    # number of hours ahead for which rainfall amounts are predicted

HOURS_IN_A_DAY = 24
NN_OUTPUTS = HOURS_AHEAD                            # number of outputs of NN
NN_INPUTS = DAYS_BACK * HOURS_IN_A_DAY * INPUTS     # number of inputs of NN (DAYS_BACK*24-1 past values and current value)

HIDDEN_NODES = int(NN_INPUTS * 2)                   # number of nodes in hidden layer (multiple of NN_INPUTS)

MAX_RAINFALL_AMOUNT = 62.4                          # representing normalized value of 1 in rainfall amount
RAINFALL_RESOLUTION = 0.1							# accuracy of the measurements of rainfall

LARGEST_ERROR_PER = 0.1								# parameter for a metrices for largest error average

LOAD_INPUTS_PATH = 'whole_model/nn_data.pk'			# path to stored values of inputs for testing purposes

#################     FUNCTIONS     #################

# get_inputs(LOAD_INPUTS_PATH)
#  returns the inputs
def get_inputs(LOAD_INPUTS_PATH):
	print("Loading nn_data_in from saved values.")
	with open(LOAD_INPUTS_PATH, 'rb') as fi:
		# load data from the file
		nn_data_in = pickle.load(fi)
		nn_data_out = pickle.load(fi)
	print("Finished loading nn_data_out.")

	nn_data_in = np.transpose(nn_data_in)

	a = nn_data_in[:,num_input_sample:num_input_sample+1]

	return nn_data_in[:,num_input_sample:num_input_sample+1]

# leaky_relu(x, alpha)
def leaky_relu(x, alpha = 0.2):
	x[x<0] = x[x<0]*alpha			# multiply the negative values with coeficient alpha
	return x

# softmax(x)
def softmax(x):
    """Compute softmax values for each sets of scores in x."""
    sums = np.sum(np.exp(x), axis=0)
    return np.exp(x) / sums[None,:]

# calc_outputs(Wi2h, Bi2h, Wh2o, Bh2o, X, stage)
#  calculate the outputs of the NN according
#  to the formulas:
#   Y = output_activfun(Wh2o*H+Bh2o) 
#   H = leaky_relu(Wi2h*X+Bi2h)
#  use parameter stage to choose appropriate activation function for output nodes
def calc_outputs(Wi2h, Bi2h, Wh2o, Bh2o, X, stage):

	# calculate the values of the hidden layer
	calc = np.dot(Wi2h, X)				# caluclate dot product of matrices

	calc = calc + Bi2h					# add bias to each element (to each element of a row add corresponding bias)

	alpha = 0.2 						# alpha parameter of the leaky relu (in tensorflow the default is 0.2 according to https://www.tensorflow.org/api_docs/python/tf/nn/leaky_relu?)
	calc = leaky_relu(calc,alpha)		# apply the leaky_relu function to each element

	# calculate the outputs of the NN
	calc = np.dot(Wh2o, calc)			# caluclate dot product of matrices
	calc = calc + Bh2o					# add bias to each element (to each element of a row add corresponding bias)

	# define the output activation function according to the type of the NN
	if stage != 'reg':				
		# in case of classification NN, the output activation function is softmax
		calc = softmax(calc)			# apply output activation function to each element
	else:
		# in case of the regression NN, thr output activation function is exp
		calc = np.exp(calc)				# apply output activation function to each element

	return calc

# recreate_model_from_bin_SVD(data, stage)
#  returns matrices Wi2h, Bi2h, Wh2o, Bh2o of a NN model
#  matrices are saved in 16 'dynamic' floating point
#  'dynamic' means that each matrix has its own number of bits for int - denoted by the first byte
#  matrix Wi2h is written as U[m x r], s[r x 1], VT[r x n] in from SVD, where r is defined at the start of this file
def recreate_model_from_bin_SVD(data, stage):

	t0 = time.time()

	num_output_nodes = nums_output_nodes[stage]			# get the number of output nodes according to the stage type

	if ENABLE_SVD:
		[U, s, VT, Bi2h, Wh2o, Bh2o] = [[],[],[],[],[],[]]	# initialize empty list for matrices
		matrices = [U, s, VT, Bi2h, Wh2o, Bh2o]				# join them under common name
		# define the sizes of each matrix [num_row,num_columns]
		matrices_sizes = [[num_hidden_nodes, r],
						  [r, 1],
						  [r, num_features],
						  [num_hidden_nodes, 1],
						  [num_output_nodes, num_hidden_nodes],
						  [num_output_nodes, 1]]
	else:
		[Wi2h, Bi2h, Wh2o, Bh2o] = [[],[],[],[]]	# initialize empty list for matrices
		matrices = [Wi2h, Bi2h, Wh2o, Bh2o]				# join them under common name
		# define the sizes of each matrix [num_row,num_columns]
		matrices_sizes = [[num_hidden_nodes, num_features],
						  [num_hidden_nodes, 1],
						  [num_output_nodes, num_hidden_nodes],
						  [num_output_nodes, 1]]						

	bin_data = data

	i = 0 									# current byte from the file which wasn't yet read & stored
	for mi, matrix in enumerate(matrices):
		N = bin_data[i]						# number of bits used for the integer part of the value
		i = i + 1 							# increase the counter to next byte

		# build a matrix of the appropriate size and fill it with values represented by corresponding bytes
		for row in range(matrices_sizes[mi][0]):
			matrices[mi].append([])			# make a new row

			tm1 = time.time()

			for col in range(matrices_sizes[mi][1]):
				# get all the bytes for one value
				byte_val = []
				for Bi in range(BYTE_NUM):
					byte_val.append(bin_data[i])	# get 8 bits
					i = i + 1						# move the bit index


				# ### NEW ALGORITHM TO RECREATE FLOATS FROM BYTES
				tmp_int = int.from_bytes(byte_val, byteorder='big', signed=False)
				if tmp_int>(1<<(BIT_NUM-1)):
					tmp_int -= (1<<BIT_NUM)

				float_val_2 = tmp_int / (float)(1<<(BIT_NUM-N-1))
				matrices[mi][row].append(float_val_2)	# append to the matrix calculated value
				# ### END OF NEW ALGORITHM TO RECREATE FLOATS FROM BYTES

			tm2 = time.time()
			#print("Building a matrix row takes: {}".format(tm2-tm1))

		# Covert data lists to numpy arrays
		matrices[mi] = np.array([ np.asarray(xi, dtype=float) for xi in matrices[mi] ])

	t1 = time.time()
	print("Recreating model takes: {}".format(t1-t0))

	if ENABLE_SVD:
		# join the first 3 matrices U, s, VT into Wi2h
		matrices[1] = np.transpose(matrices[1])[0]		# to transpose s into vector
		Wi2h = np.dot(matrices[0], np.dot(np.diag(matrices[1]), matrices[2]))
		#print(Wi2h)
		#exit()
		return [Wi2h, matrices[3], matrices[4], matrices[5]]
	else:
		#print(matrices[0][:10,0])
		#exit()
		return matrices

#################      PROGRAM      #################

print("Starting program.")

tstart = time.time()

model_path = 'whole_model/hour_{hour}_model/{stage}_{form}'		# path to the NN for hour {hour} and NN of stage {stage} in form of {form} - form is either QUANT_FORM, CSV_FORM or TENS_FORM
QUANT_FORM = 'nn_quant.bin'										# quantized form of the model weights

print("Getting inputs.")
# Get the inputs
X = get_inputs(LOAD_INPUTS_PATH)

nn_data_out_pred = [] #for saving the results of the prediction

zf = zipfile.ZipFile('whole_model.zip')	#zipfile of the whole model

# Evaluate the compressed model for each hour and each its NN stage
# and compare it to the reference/most accurate model 
for hour in range(1,HOURS_AHEAD+1):
	print("Evaluating for hour {}.".format(hour))

	# Get results of bin stage
	stage = 'bin'
	data = zf.read(model_path.format(hour=hour, stage=stage, form=QUANT_FORM))
	Wi2h, Bi2h, Wh2o, Bh2o = recreate_model_from_bin_SVD(data, stage)
	Y_bin = calc_outputs(Wi2h, Bi2h, Wh2o, Bh2o, X, stage)
	Y_bin = np.argmax(Y_bin, axis=0)

	# if rain is predicted - from the bin (0) NN
	if Y_bin>0:
		# Get results of multi stage
		stage = 'multi'
		data = zf.read(model_path.format(hour=hour, stage=stage, form=QUANT_FORM))
		Wi2h, Bi2h, Wh2o, Bh2o = recreate_model_from_bin_SVD(data, stage)
		Y_multi = calc_outputs(Wi2h, Bi2h, Wh2o, Bh2o, X, stage)
		Y_multi = np.argmax(Y_multi, axis=0)
		# if the rainfall is from the last interval - from the multi (1) NN
		if Y_multi==3:
			# Get results of reg stage
			stage = 'reg'
			data = zf.read(model_path.format(hour=hour, stage=stage, form=QUANT_FORM))
			Wi2h, Bi2h, Wh2o, Bh2o = recreate_model_from_bin_SVD(data, stage)
			Y_reg = calc_outputs(Wi2h, Bi2h, Wh2o, Bh2o, X, stage)
			Y_reg = Y_reg[0][0]
			# and set the appropriate regression value to the output - from the reg (2) NN
			tmp = Y_reg
		else:
			# turn class label into average value for the interval
			tmp = (Y_multi[0]+0.5)/MAX_RAINFALL_AMOUNT
	else:
		tmp = 0

	nn_data_out_pred.append(tmp*MAX_RAINFALL_AMOUNT)
	#print(nn_data_out_pred[hour-1])
	print("Rainfall prediction for hour {hour}: {rain:.1f} mm".format(hour=hour, rain=nn_data_out_pred[hour-1]))

print("\nPrediction for 24 hours ahead:")
print(nn_data_out_pred)



### ADVISING ON WATERING
print("\nAdvising on watering:")
# Depending on the soil moisture conent advise the user on watering
current_moist = get_current_moist()		# get current levels of moisture content - higher value, lower moisture!

#print(MAX_OPTIMAL_MOIST_ADC)
#print(MIN_OPTIMAL_MOIST_ADC)

current_moist = 875

# If the moisture is above optimal level, do not water
if current_moist<MAX_OPTIMAL_MOIST_ADC:
	print("You shouldn't water the plants as the soil is already too wet.")
else:
	# Depending on the rainfall prediction for the next hours advise how much should be watered
	dif = [x - WATER_NEEDED_PER_DAY_MM/24 for x in nn_data_out_pred]		# difference between the amount that will fall in the next hours and water consumed by the plants

	c_little = 0
	# count the next consequitve hours when there is too little rain
	for d in dif:
		if d<0:
			c_little += 1
		else:
			break

	# see if there is any reserve of water in the soil
	if current_moist<=MIN_OPTIMAL_MOIST_ADC:
		rainfall_reserve = MOIST_TO_RAINFALL(current_moist)
		will_need_rainfall = sum(dif[0:c_little])

		#print((MAX_WATER_RESERVE*WATER_NEEDED_PER_DAY_MM/24 - WATER_NEEDED_PER_DAY_MM/24)/(MAX_OPTIMAL_MOIST_ADC - MIN_OPTIMAL_MOIST_ADC))
		#print(current_moist-MAX_OPTIMAL_MOIST_ADC)
		#print(MAX_WATER_RESERVE*WATER_NEEDED_PER_DAY_MM/24)

		#print(rainfall_reserve)
		#print(will_need_rainfall)

		if rainfall_reserve + will_need_rainfall < 0:
			# there is too little reserve of water, thus start watering
			print("You should water the plants as there is not enough reserve in the soil.")
		else:
			print("You shouldn't water the plants as there is enough reserve in the soil.")
	else:
		if c_little==0:
			print("You shouldn't water the plants although there isn't any water in the soil but it will rain.")
		else:
			# if there is no reserve and there will be not enough rain, you should water the plants
			print("You should water the plants as there isn't any water in the soil and it will not rain.")
###

tend = time.time()
print("Whole programe takes: {}".format(tend-tstart))