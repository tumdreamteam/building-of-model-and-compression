# Program does copression of NN weights and biases
# for all hours of the model. It enables value
# quantization, Singular Value Deposition (SVD)
# and zipping of the compressed files.
# 
# Quantization is done
# to fixed floating point with BIT_NUM bits altogether.
# BIT_NUM is a multiple of 8.
# Of the BIT_NUM bits, 1 is used for sign (MSB), N for 
# the integer part and M for the floating part.
#
# SVD is done
# only on the largest of the matrices (Wi2h[m x n]).
# The matrix is compressed according to value 'r'.
# The compression ratio is: m*n/(m*r + r + r*n).
#
# At the end, the compressed binary files are zipped.
#
# To store the values and to save as much space,
# some data is not included in the quantized files
# but assumes the next structure (have to be the
# same while reading and writing to such a file):
#  - there are for parts of the model description,
#	 namely matrices Wh2o, Bh2o, Wi2h, Bi2h where (of size [rows x columns]):
#     Wh2o - weights from the hidden to the output layer [num_output_nodes x num_hidden_nodes]
#     Bh2o - biases for the hidden to the output layer [num_output_nodes x 1]
#     Wi2h - weights from the inputs to the hidden layer [num_hidden_nodes x num_features]
#     Bi2h - biases for the inputs to the hidden layer [num_hidden_nodes x 1]
#  - each part consists of the following bytes:
#	  1st byte - unsigned int presenting N (number of integer bits from the BIT_NUM bit values)
#	  following i and i+1 bytes - signed int value of weights/biases where
#	   i goes from 1 to rows*columns*2 by a step of 2
#  - altogether there should be (r_Wh2o*c_Wh2o + r_Bh2o*c_Bh2o + r_Wi2h*c_Wi2h + r_Wi2h*c_Wi2h)*2 + 4 bytes
#
#
# Author: Uros Hudomalj
# Last revision: 29.12.2018

#################      IMPORTS      #################
import numpy as np
import csv
from scipy.linalg import svd
from sklearn.utils.extmath import randomized_svd
import time
import zipfile
import zlib

#######    CONSTANTS and GLOBAL VARIABLES    ########

HOURS_AHEAD = 24        # number of hours ahead for which rainfall amounts are predicted
BYTE_NUM = 2			# number of bytes used for storage of a single value
BIT_NUM = BYTE_NUM*8	# number of bits used for storage of a single value - has to be a multiple of 8

ENABLE_SVD = False		# if true, SVD is also used for copression
K = 1.01				# for SVD compression of m*n/(m*r + r + r*n) - used just on Wi2h [m x n]
r = int(191.78/K)		# in our example m=2n => 2n*n/(r*(3n+1)) and n=288 => r = 191.78/K
print("Value 'r' of SVD: {}".format(r))

#################     FUNCTIONS     #################

# read_csv_model(model_path)
#  reads the weights and biases of the model from a CSV
def read_csv_model(model_path):

	with open(model_path, 'r') as csv_file:
		data = list(csv.reader(csv_file, delimiter=';'))

	Wi2h = []
	Bi2h = []
	Wh2o = []
	Bh2o = []

	# from th input CSV divide data into the right matrices
	idx = 0
	for row in data:
		if row[0] == 'Weights from input to hidden layer.':
			idx = 1
		elif row[0] == 'Bias from input to hidden layer.':
			idx = 2
		elif row[0] == 'Weights from hidden to output layer.':
			idx = 3
		elif row[0] == 'Bias from hidden to output layer.':
			idx = 4
		else:
			if idx == 1:
				Wi2h.append(row)
			elif idx == 2:
				Bi2h.append(row)
			elif idx == 3:
				Wh2o.append(row)
			elif idx == 4:
				Bh2o.append(row)

	# Covert data lists to numpy arrays
	Wi2h = np.array([ np.asarray(xi, dtype=float) for xi in Wi2h ])
	Bi2h = np.array([ np.asarray(xi, dtype=float) for xi in Bi2h ])
	Wh2o = np.array([ np.asarray(xi, dtype=float) for xi in Wh2o ])
	Bh2o = np.array([ np.asarray(xi, dtype=float) for xi in Bh2o ])

	# Transpose the matrices to get the right dimenssions
	Wi2h = np.transpose(Wi2h)
	Bi2h = np.transpose(Bi2h)
	Wh2o = np.transpose(Wh2o)
	Bh2o = np.transpose(Bh2o)

	return [Wi2h, Bi2h, Wh2o, Bh2o]

# stochastic_round(x)
#  returns value rounded using stochastic rounding
def stochastic_round(x):
	np.random.seed(0)			# set the seed of the rand generator to the same value to get same results each run

	sign = np.sign(x)
	x = np.abs(x)
	is_up = np.random.random(x.shape) < x-np.floor(x)

	x[is_up] = np.ceil(x[is_up])
	x[~is_up] = np.floor(x[~is_up])

	#round_func = np.ceil if is_up else np.floor
	#return sign * round_func(x)

	return sign * x

# quantize_matrix(matrix)
#  returns a matrix with weights quantized to a
#   representation of BIT_NUM bits fixed floating point
#  also returns N - number of bits used for the integer
#   part of the quantized float value
def quantize_matrix(matrix):
	# get the absolute max value in the matrix to determine the number of bits for the integer part
	max_val = np.max(np.abs(matrix))
	max_val = max_val if max_val>1 else 1 		# otherwise the log2 gives negative numbers if max_val is less then 1
	N = np.uint8(np.ceil(np.log2(max_val)))

	multi = 1<<(BIT_NUM-N-1) 					# multiply every number by a constant of 2**(BIT_NUM-N-1) which is max number from bits of the floating part
	matrix = matrix*multi 						# to move the decimal point

	matrix = stochastic_round(matrix)			# round the remaining decimal portion
	matrix = matrix.astype(dtype='int32')		# now the numbers are in range of the int BIT_NUM and can be converted

	return N, matrix

# write_bin_model(matrices, quantized_model_path)
#  write all the matrices to a bin file
#  matrices[0] - byte representing used number of bits
#   for the integer part of the values
def write_bin_model(matrices, quantized_model_path):
	with open(quantized_model_path, 'wb') as bin_file: 
		# write each element of matrices to bin_file
		for matrix in matrices:
			#print("IP bits {}".format(matrix[0]))
			bin_file.write(matrix[0])						# write the 1st byte denoting number of bits for integer part
			for row in matrix[1]:
				for element in row:
					for Bi in range(BYTE_NUM,0,-1):
						B_val = np.uint8((element>>8*(Bi-1))%256) 	# get the Bi-th 8 bits
						bin_file.write(B_val)						# write 8 bits of weigth/bias

# zip_files(what, to_where)
#  zip and compress all the files in ''what to location at 'to_where'
def zip_files(what, to_where):
	compression = zipfile.ZIP_DEFLATED
	zf = zipfile.ZipFile(to_where+'.zip', mode='w')
	for fi in what:
		zf.write(fi, compress_type=compression)
	zf.close()


#################      PROGRAM      #################

print("Starting program.")

model_path = 'whole_model/hour_{hour}_model/{stage}_{form}'		# path to the NN for hour {hour} and NN of stage {stage} in form of {form} - form is either QUANT_FORM, CSV_FORM or TENS_FORM
nn_stages = ['bin', 'multi', 'reg']								# stages/types of NN for a model for an hour
QUANT_FORM = 'nn_quant.bin'										# quantized form of the model weights
CSV_FORM = 'nn_weights.csv'										# reference/accurate model weights in CSV form
TENS_FORM = 'model'												# nn model is saved in a format native to Tensorflow

zip_list = []
for hour in range(1,HOURS_AHEAD+1):

	for i_stage, stage in enumerate(nn_stages):
		print("Getting accurate model weights and biases for hour {hour} and stage {stage}.".format(hour=hour, stage=stage))
		# Read the model weights and biases
		Wi2h, Bi2h, Wh2o, Bh2o = read_csv_model(model_path.format(hour=hour, stage=stage, form=CSV_FORM))

		if ENABLE_SVD:
			# Compress the largest matrix - Wi2h, using SVD method
			if r > min(Wi2h.shape): r = min(Wi2h.shape)		# a full/lossless reconstruction possible
			U, s, VT = randomized_svd(Wi2h, r)
			#U, s, VT = svd(Wi2h, full_matrices=False)
			#U, s, VT = U[:,:r], s[:r], VT[:r,:]
			A = np.dot(U, np.dot(np.diag(s), VT))
			s = np.transpose(s[np.newaxis])					# to get the vector s in the same format as Bi2h
			diff = A - Wi2h
			print(" Average error due to SVD {:.2f}%".format(np.average(np.abs(diff))*100/np.average(np.abs(Wi2h))))
			matrices = [U, s, VT, Bi2h, Wh2o, Bh2o]
		else:
			matrices = [Wi2h, Bi2h, Wh2o, Bh2o]

		for i, matrix in enumerate(matrices):
			print("  Quantizing matrix - step {}.".format(i+1))
			# Quantize the matrix
			matrices[i] = quantize_matrix(matrix)

		print("Writing quantized model weights and biases.")
		# Write quantized model weights and biases
		write_bin_model(matrices, model_path.format(hour=hour, stage=stage, form=QUANT_FORM))
		zip_list.append(model_path.format(hour=hour, stage=stage, form=QUANT_FORM))

print("Zipping whole model.")
# Zip whole model
to_where = 'whole_model'
zip_files(zip_list, to_where)